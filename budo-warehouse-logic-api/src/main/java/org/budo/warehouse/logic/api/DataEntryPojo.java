package org.budo.warehouse.logic.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.budo.warehouse.logic.util.DataEntryUtil;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 可序列化可传输的简单对象
 * 
 * @author limingwei
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class DataEntryPojo implements DataEntry, Serializable {
    private static final long serialVersionUID = 1648582679159852643L;

    private Long executeTime;

    private String eventType;

    private String sql;

    private String schemaName;

    private String tableName;

    private List<Row> rows;

    public DataEntryPojo(DataEntry dataEntry) {
        this.setEventType(dataEntry.getEventType());
        this.setSql(dataEntry.getSql());
        this.setSchemaName(dataEntry.getSchemaName());
        this.setTableName(dataEntry.getTableName());

        List<Row> _rows = DataEntryUtil.toRows(dataEntry);
        this.setRows(_rows);
    }

    @Override
    public Integer getRowCount() {
        return this.getRows().size();
    }

    @Override
    public Integer getColumnCount(Integer rowIndex) {
        return this.getRows().get(rowIndex).getColumns().size();
    }

    @Override
    public String getColumnName(Integer rowIndex, Integer columnIndex) {
        return this.getRows().get(rowIndex).getColumns().get(columnIndex).getName();
    }

    @Override
    public Boolean getColumnIsKey(Integer rowIndex, Integer columnIndex) {
        return this.getRows().get(rowIndex).getColumns().get(columnIndex).getIsKey();
    }

    @Override
    public String getColumnValueBefore(Integer rowIndex, Integer columnIndex) {
        return this.getRows().get(rowIndex).getColumns().get(columnIndex).getValueBefore();
    }

    @Override
    public String getColumnValueAfter(Integer rowIndex, Integer columnIndex) {
        return this.getRows().get(rowIndex).getColumns().get(columnIndex).getValueAfter();
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    public static class Row implements Serializable {
        private static final long serialVersionUID = -1166921999979903866L;

        public Row(DataEntry dataEntry, Integer rowIndex) {
            Integer columnCount = dataEntry.getColumnCount(rowIndex);
            List<Column> _columns = new ArrayList<Column>(columnCount);
            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                _columns.add(new Column(dataEntry, rowIndex, columnIndex));
            }

            this.setColumns(_columns);
        }

        private List<Column> columns;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    public static class Column implements Serializable {
        private static final long serialVersionUID = -357503420964857479L;

        public Column(DataEntry dataEntry, Integer rowIndex, Integer columnIndex) {
            this.setName(dataEntry.getColumnName(rowIndex, columnIndex));
            this.setIsKey(dataEntry.getColumnIsKey(rowIndex, columnIndex));
            this.setValueBefore(dataEntry.getColumnValueBefore(rowIndex, columnIndex));
            this.setValueAfter(dataEntry.getColumnValueAfter(rowIndex, columnIndex));
        }

        private String name;

        private Boolean isKey;

        private String valueBefore;

        private String valueAfter;
    }
}