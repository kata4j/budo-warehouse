package org.budo.warehouse.service.api;

/**
 * @author lmw
 */
public interface JdbcExecuteService {
    Integer executeUpdate(Integer dataNodeId, String sql, Object[] parameters);
}